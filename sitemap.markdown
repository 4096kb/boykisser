---
layout: page
titletag: Sitemap
title: Pages by Tag
desc: "Sitemap for 4096kb.gay. This page will serve as your guide to all things 4096kb, at least those that are published. By me."
permalink: /sitemap
---
{% for each in site.tags %}
<section markdown="0">
<h2 id="sect-{{each[0]}}">Posts Tagged <b><u>{{ each[0] }}</u></b></h2>
{% if site.data.tags contains each[0] %}
<p><i>{{ site.data.tags | map: each[0] }}</i></p>
{% endif %}
<ul>
{% for pg in each[1] %}
<li><a href="{{pg.url}}">{{pg.title}}</a>
    <ul>
        <li>Content: {{pg.desc}}</li>
        <li>Published: {{pg.date | date: "%Y-%m-%d"}}</li>
    </ul>
</li>
{% endfor %}
</ul>
</section>
{% endfor %}
