---
layout: page
titletag: Wiki
title: "Wiki"
desc: "A wiki-style section of my site dedicated to catalouging information regarding interesting or notable info on devices I've tinkered with"
primcolor: "#919191ab"
iswikiindex: true
---

<section markdown="1">

## Overview

Welcome to the wiki! I've found that sometimes I want
to write something that's not well contained in a blog post,
so this section of the site is a wiki-esque area where I
can write anything I feel fits that criteria.

### Organization

Usually, things are organized as `/wiki/manufacturer/device`. Each device
has has an index page which links to the content present for the device.

### Contributing

If you find that a page is missing info or you'd like to add to what I 
have here, I'd love to hear your input! Either create a pull request
[on the website's GitLab repo](https://gitlab.com/4096kb/boykisser) or 
email me at [contributions@4096kb.gay](mailto:contributions@4096kb.gay).
Depending on how much you're contributing, we'll mark you as a page author
if you've written a whole page or we'll mark all editors as contributors
if you're modifying an existing page.

I license all my work on this site as Creative Commons BY, so I unfortunately
won't be accepting any contributions if you're not alright with this 
license. I'm happy to add a link to your own webpage though in the interest
of providing complete information on a topic if you want to license differently
or want more control over the presentation of the relevant info.

### Stylization Guidelines

Wiki pages should be stylized as follows:

1. No or minimal custom HTML. If tweaks to HTML or CSS are needed, they should
   be implemented site-wide.
2. Semi-formal writing should be used. This includes minimal profanity, a strict
   third-person perspective, proper spelling and capitalization, and logical flow
   of information.
3. If a page feels too long, it probably is. 

</section>

<section markdown="1">

## Wiki Index Entries

| Page | Topic Covered |
|------|---------------|
{% for each in site.pages %}{% if each.iswikiindex == true and each.title != page.title %}| [{{each.url}}]({{ each.url }}) | {{ each.desc }} |
{% endif %}{% endfor %}

</section>

