---
layout: page
titletag: MyArcade Gamestation Pro Specs
title: "MyArcade Gamestation Pro: Specifications"
desc: "Exploring the specs of the 2023 MyArcade Gamestation Pro, which carries a Rockchip RK3032 SoC and has the serial GBX107-V1.3 on the board."
primcolor: "#840f0fab"
---

<section markdown="1">

## Overview

The following is a record of hardware observed in a MyArcade 
Gamestation Pro model DGUNL-7052 obtained in late December 2023.

| Component             | Vendor    | Model  | Component Specifics               |
|-----------------------|-----------|--------|-----------------------------------|
| [SOC](#soc)           | Rockhchip | RK3032 | ARMv7 architecture, dual-core     |
| [Storage](#storage)   | Winbond   | W25N02KVZEIR | 2 Gigabit SPI NAND Flash    |
| [RAM](#ram)           | Samsung   | K4B2G1646F-BCMA | 2 Gigabit capacity, DDR3 |
| [Motherboard](#motherboard) | Shenzhen Sunchip Technology Co., Ltd. | GBX107-V1.3 ||

</section>

<section markdown="1">

## SOC

![The RK3032 SoC.](/assets/posts/myarcadedoom/components_fig01.webp){:.centered}

Datasheet source: [sekorm.com](https://en.sekorm.com/doc/3404423.html)

Chip markings transcription:

```
[Rockchip Logo]
RK3032
NBAUM5487 2311
```

According to the datasheet cited, the final string on the
chip marking is coded as follows:

```
ABCXXXXXX DEFG

ABC:    Subcontractor Code
XXXXXX: Die Lot No#
DEFG:   Date Code
```

Given this, we have the following info about the SoC present
in the sample unit:

| Field         | Observed Value |
|---------------|----------------|
| Subcontractor | `NBA`          |
| Die Lot No#   | `UM5487`       |
| Date Code     | `2311`         |

Specs I found of interest from the linked datasheet:

| Feature | Shipped spec. |
|---------|---------------|
| Architecture | ARM Cortex-A7, 32-bit |
| Core Count   | Dual core     |
| Caches | 32KB L1 instruction, 32KB L1 data, 128KB L2 |
| On-chip Storage | 16KB Bootrom, 8KB internal SRAM |
| Max Clock Speed | 1.0GHz |
| Temperature Operating Range | Recommended 0°C < t < 80°C, max 125°C |
| RAM Support | DDR3/DDR3L 533MHz, 2GB max |
| GPU | Mali 400 MP1 |
| GPU Max Supported Resolution | 1080p encode/decode |
| GPU Min Supported Resolution | 480i |
| GPU HDMI Revision | HDMI 1.4a |
| GPU HDCP Support | HDCP 1.2 |
| GPU Codecs | H.263, H.264, HVEC/H.265, MPEG-[1, 2, 3, 4], VP8, MVC, JPEG |
| GPU Supported Graphics APIs | OpenGL ES1.1, OpenGL ES2.0, OpenVG1.1 |
| Max GPU Clock Speed | 400MHz |
| Audio Interface | 8 channel PCM, 16/20/24 bits |
| Connectivity | SPI, UART, I2C, GPIO, USB OTG |

</section>
<section markdown="1">

## Storage

![The Winbond SPI chip, part number 25N02KVZEIR.](/assets/posts/myarcadedoom/components_fig02.webp){:.centered}

Datasheet source: [digikey.com](https://www.winbond.com/resource-files/W25N02KVxxIRU_Datasheet_20210625_G.pdf)

Chip markings transcription:

```
[winbond Logo]
25N02KVZEIR
2314
6135FC6AS02
```

Specs of interest:

| Feature | Shipped spec. |
|---------|---------------|
| Capacity | 2 Gigabits (256 Megabytes) |
| Top Speed | 50MB/s |
| Rated Erase/Program Cycle Count | 60,000 times |
| Rated Data Retention Time | 10 years |
| Temperature Operating Range | -40°C < t < 85°C |
| Other Notable Features | 8-Bit ECC, Write Protection |

Chip pinout:
```
               +--------+
1)        /CS -| *      |- VCC          (8
2)  DO (IO_1) -|        |- /HOLD (IO_3) (7
3) /WP (IO_2) -|        |- CLK          (6
4)        GND -|        |- DI (IO_0)    (5
               +--------+
```
The chip can be temporarily disabled by shorting CLK (Pin 6)
to ground like so:

![A pair of tweezers bridges the second pin from the bottom out of four on the right side of the Winbond SPI to the ground trace that runs around the mainboard.](/assets/posts/myarcadedoom/maskrom_fig01.webp){:.centered}

</section>
<section markdown="1">

## RAM

![The Samsung DDR3 memory module.](/assets/posts/myarcadedoom/components_fig03.webp){:.centered}

Datasheet source: [samsung.com](https://download.semiconductor.samsung.com/resources/product-guide/DDR3_Product_guide_Oct.16[2]-0.pdf)

Chip markings transcription:

```
SEC 248
K4B2G16
46F BCMA
```

Specs of interest:

| Feature | Shipped spec. |
|---------|---------------|
| Density | 2 Gigabits (256 Megabytes) |
| Banks | 8 |
| Layout | 128Mb x 16 |
| Package | 96 ball FPGA | 
| Operating Voltage | 1.5V |
| Temperature Operating Range | 0°C < t < 85°C |
| Speed | 1866 Mbps |

</section>
<section markdown="1">

## Motherboard

![The main board.](/assets/posts/myarcadedoom/overview_fig10.webp){:.centered}

![Silkscreened Model Number](/assets/posts/myarcadedoom/components_fig05.webp){:.centered}

Transcription of silkscreened model information:
```
GBX107-V1.3
2022-12-23
```

![Another Model Number, this time on a sticker](/assets/posts/myarcadedoom/components_fig06.webp){:.centered}

Transcription of the information on the sticker:
```
GBX107-V1.3
CXPOHS23098
2023-07-03
```

An observation one may make here is that the board's silkscreened
date is roughly six and a half months before the date on the sticker.

There's no documentation for this online, so what's here is what has simply
been observed in passing. As seen in [captured dmesg output](/assets/posts/myarcadedoom/dmesg.log),
the kernel for this system was compiled by user `sunchip` on `sunchip-PowerEdge-R740`.
Searching for *sunchip rockchip* points to a company [Shenzhen Sunchip Technology Co., Ltd](https://www.embeddedsystemboard.com/)
as being a possible manufacturer. Sunchip manufactures boards for plenty of systems, mostly using
Rockchip SoCs. Boards created by Sunchip bear a good resemblance to the one present in the
Gamestation Pro.

Looking for other boards with similar model information, one may
stumble across some Arcade1Up boards. Looking into
[u/BerryBerrySneaky's post on r/Arcade1Up](https://old.reddit.com/r/Arcade1Up/comments/kbc2u3/arcade1up_hardware_generation_comparison/),
one may notice that Generation 5a uses the same Rockchip RK3032
SoC and carries the model number `GBX70-V01`. The board is also visually 
similar to what's shipped in the Gamestation Pro.

</section>

