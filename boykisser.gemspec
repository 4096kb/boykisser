# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "boykisser"
  spec.version       = "0.1.0"
  spec.authors       = ["4096kb"]
  spec.email         = ["me@4096kb.gay"]

  spec.summary       = "Theme for 4096kb.gay."
  spec.homepage      = "https://4096kb.gay"
  spec.license       = "CC BY-NC-SA 4.0"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_data|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.3"
end
