---
layout: null
title: Site Post Feed
titletag: RSS
---
<?xml version="1.0" encoding="utf-8"?>
<!--
Adapted from Pretty Feed v3
https://github.com/genmon/aboutfeeds/blob/main/tools/pretty-feed-v3.xsl
-->

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html lang="en">
      {% include htmlhead.html %}
      <body>
        {% include header.html %}
        <div id="content">
            {% include lmarg.html %} 
            <div id="contmain">
                {% include articlein.html %} 
                    <section>
                        <h2>RSS Info</h2>
                        <p>This page is an RSS feed! Any new post I make on this site will appear here. You can also import this list into any RSS reader of your choice to get notified when I post. Just copy <code>{{ site.url }}/rss.xml</code> into your RSS reader!</p>
                    </section>
                    <xsl:for-each select="/rss/channel/item">
                        <section>
                            <h2>
                                <a target="_blank">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="link"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </h2>
                            <p>
                            Published: <xsl:value-of select="pubDate" />
                        </p>
                            <p>
                                Description: <xsl:value-of select="description"/>
                            </p>
                            <p>Categories:<xsl:for-each select="category">
                                    <xsl:variable name="cat"><xsl:value-of select="."/></xsl:variable>
                                    <xsl:value-of select="' '"/>
                                    <a class="taglink" href="{{site.url}}/sitemap#sect-{$cat}"><xsl:value-of select="."/></a>
                                    </xsl:for-each>
                            </p>
                        </section>
                    </xsl:for-each>
                {% include articleout.html %}
            </div>
            {% include sidematter.html %} 
        {% include rmarg.html %}    
        </div>
        {% include footer.html %}
        </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
