---
layout: page
titletag: Home
title: Welcome to the :3 Zone Bitch
desc: "Homepage for 4096kb, the queer linux-using extraordinaire himself. A more blog-oriented site, I semi-frequently post on technology and modding." 
permalink: /
---
<section markdown="1">

## About Me

Hello, I'm 4096 Kilobyte! I'm currently in college on year three of a Computer Science bachelors. 
I like modding video game consoles, Linux, webdev, light graphic design, hell, anything that falls under 
computing. Gay RHEL apologist.

As for my skills, I'm still learning. I've got a firm footing in Python currently and am hoping to learn more on 
other languages too. Hopefully something compiled, like Rust or C++. I've also got at least a little knowledge of Excel,
so that's handy. As for web stuff, I'm only acquainted with plain HTML and CSS. Don't do much JavaScript and don't really 
want to use any frameworks. I've also got a little experience using Jekyll as evidenced by this site. 
Looking into learning more about systems administration too.
Currently working on getting acquainted with both Proxmox and Active Directory.

</section>
<section markdown="1">

## State of This Site

Sort of on hiatus here. Depression's kicking my ass so I haven't had the drive to write any new articles here. I've updated the index
for 2025 though, so *yaaay*. Baby steps.

</section>

