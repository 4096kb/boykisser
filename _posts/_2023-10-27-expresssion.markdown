---
layout: page
titletag: Expression
title: A Reflection on Expression of Self
desc: I write on my experience with fears of self expression caused by stereotypes.
date: 2023-10-27
permalink: /posts/expression
tags: [4096kb, lifestyle, reflection]
---
<section markdown="1">

## Hell's this about?

This post is a deviation from what I initially intended on posting on this site.
It's going to discuss some thoughts I have on expression of self. I'd like to discuss the 
following:

 - How do I define expression of self & freedom of expression?
 - How would I like to express myself?
 - Why don't I express myself?

Additionally, I'm no longer comfortable keeping this post published. I'm removing it from rendering in Jekyll.
Of course, you'll still be able to see this in the repo or changelogs. If you're reading this, I can't really stop you, 
but this isn't published for a reason.

Fair warning, this piece is likely to be tangential and spiral onto different courses.
I'm not a journalist, just some gay nerd on the Internet.

> CW: Discussions of stereotypes harmful to the LGBTQ community

</section>
<section markdown="1">

## Defining Concepts

In case I'm wrong on some definitions, I'd like to start by defining what I interpret to be 
the meaning of some terms I'm going to use.

1. Expression: The image one outwardly presents. Can be contextual, I.E. a different mask 
   for a given scenario.

2. Expression of Self: A form of expression, but maskless. Expressing your innermost self. 
   Doesn't necessarily need to be a naked presentation of your true self, can be more of a 
   window.

3. Freedom of Expression: One's control over how they express themselves.

> Sidenote here: I find myself doing stuff like this a lot, where I doubt the information I have on hand. 
  I often worry that the info I remember isn't correct, so I spend extra time doubling back 
  to ease my worries. It's almost obsessive. This may be something I have to work on in the
  future.

</section>
<section markdown="1">

## What I Want VS What I Do

Now that we've talked over these quote-unquote definitions, I'd like to outline how this applies to me.
Let's start a table with some examples.

| What I want to express | What I do express |
|---------------------|-------------------------------|
| My hobbies, such as my interest in Linux, servers, etc. | Nothing really. I actively brush off any comments about my gear or setup and only really engage if I find someone with similar interests, leaving me apparently hobby-less and boring. Even if I do find a likeminded person, I keep pretty reserved in fear of embarrasing myself. |
| Fashion past what's normalized for midwestern men: Earrings, crazy hair, etc. | Hat and hoodie. Maybe a coat in the winter. I stick with what's tried and true. I've only recently started growing out my hair, but until about a year ago, I kept it short. |
| My opinion on when someone's reached a conclusion different than mine, academic, political, or otherwise. | A noncommitted answer just hinting at want I think. Not firmly stated. Often, this ends up putting myself and the other party on diverging paths. |
| Everything that comes with not being gay and a gender expression past what's traditionally masculine. | Heterosexual male qualities. |

What I believe to be the pattern with most of my expression habits, as can be seen here, is that I often 
clamp my expression to that of what I observed to be "Normal" expression as I grew up.

</section>
<section markdown="1">

## Why Fit The Mold?

I've clearly got my own identity and hobbies, so why do I force myself to fit the mold? I've thought about
this question on and off for the past few weeks and I think it has something to do with a fear of ridicule 
or being judged. 

Going back to the Linux example, I'd like to recall an interaction I recently had. A classmate had 
inquired about my laptop, a Lenovo Thinkpad T430, which was a gift from my younger brother. It's running Arch Linux, 
is Librebooted, and has the T420 keybaord. Clearly, this is an item that means a lot to me. The classmate had said that 
my computer was neat, but rather than talk about it and what I liked in it, I simply brushed it off as "Yeah, it's an 
older computer, but it does get me along". 

A pattern I've noticed recently is that I do things like this when they pertain to hobbies 
that I've seen fall under scrutiny. For example, Free and Open Source Software, Anime and Manga, and Homosexuality are 
three big parts of my identity. However, damaging stereotypes often make me bottle these parts of 
my identity up. I'd love to talk about Linux with my coworkers, but I worry they'll see me as 
an "Uhm, it's ahctually GNU+Linux sweaty" type user. Same goes for anime and manga, where I worry 
my peers will see me as some weeb who's failed to touch grass in the last 6 days. 
</section>
<section markdown="1">

## Thinking Further

So wow, that's totally me falling victim to stereotypes! I couldn't really put to words *why*
I felt under shame for my sexuality and hobbies, but when my therapist brought up stereotypes 
during a session, that sort of planted a seed for these thoughts.

I've been doing some closer reflection on why these stereotypes cause me to bottle up and I think I've got a 
start on a more articulated answer. Let's double back to my sidenote in the first section. I think this behavior where 
I don't stick to my guns may be a sizable factor in this equation. I know what I am and what I like, but when challenged,
I tend to backpedal, avoid accusing the other person of being wrong. 

Let's get an example of this. I like to occasionally sit down and watch anime when I've got the time to. 
I also enjoy reading Chainsaw Man as well as *🌸gay romance webcomics🌸*. 
Of course, this can be paralleled to normal media consumption habits. An analogue to this could be turning on 
Netflix instead and watching some true crime series, then flipping through the Twilight trilogy afterwards (or something like that - never read this).
However, the stereotypes associated with male weebs dictate that I must be a basement dwelling degenerate slobbering over
underaged girls. While the former part may be true, I don't want to be seen as either of these. 
In the center of this lies an internal struggle between my sense of pride and a desire to avoid conflict. So,
given the choice between ignoring stereotypes, refuting stereotypes, or avoiding the topic,
I choose the latter, hiding away this part of my identity away unless I'm engaged in conversation with 
someone who's on the in-group, someone who will know that these damaging stereotypes are bogus.
</section>
<section markdown="1">

## The Homosexual Stereotypes

The same is true for my homosexuality, and to a deeper extent as well. Paraphrasing from ContraPoints, a 
YouTuber who covers politics and philosophy often focused around LGBTQ issues, an attack on one's 
sexual orientation is much more severe than an attack on many other attributes of their self
because this is an immutable factor of their person. I could always stop using Arch Linux and give up my love 
for Neon Genesis Evangelion, but chances are low that I'll stop being attracted to men any time soon. 
This makes stereotypes on homosexuality much worse in comparison to the other ones I've listed. 

Unfortunately, the stereotypes on homosexuals and transgender individuals are much more harmful 
than the other ones I've listed. 
One that I've been hearing a lot lately is about "groomers". If you've been living under a rock, 
this is a term that right-wing individuals will use to disparage LGBTQ folks. It's based around 
the idea that the community is seeking to "brainwash" children by invading public spaces 
where children are common. Think drag queen story hour or pride parades. The label is often tied 
to accusations of "conversion"/"recruitment" or pedophilia.

Because of this stereotype, I no longer feel comfortable in places where kids are. 
Do I want to be seen as someone who wants to diddle your kids? Hell fucking no! God! 
I just want some dude to cuddle with, but because of this stereotype, I get anxiety 
even just shopping for groceries. The fear of being labeled a pedophile absolutely
paralyzes me, even if I know it's completely baseless.
</section>
<section markdown="1">

## Moving Past This

These feelings are obviously Not Good Feelings, so what are some ways I can move past this? Let's do another table.

| Option | Opinion |
|---|---|
| 1. Ignore stereotypes / Thick skin mentality | I'm not a big fan of this one. Inaction on something like this would really piss me off because at one point that's just compliance with people's fucked up worldviews, a totally enlightened-centrist take. I'm also a super emotional person. I don't think I'd be able to wear a thick skin. |
| 2. Avoid places where stereotypes propogate | Also not a fan of this one. Yes, doing this would probably do me some good mental-health wise, however, I think that long-term, this would be a really bad idea. Ignoring trends of bigotry may lead to an inability to recognise danger. These people love to use specific verbiage and dogwhistles, so knowing these is something that's vital to avoid associating with assholes. |
| 3. Bottle up and/or repress | I.E. the continued inaction route. Been on this train for two years, NO THANKS! I want to be gay and I want to be comfortable being gay. |
| 4. Refute stereotypes | Wow, who would have fucking guessed. Let's expand on this in a new section. |

</section>
<section markdown="1">

## Refuting Stereotypes

OK. Refuting is probably the best option. But how do we get there? There's a few steps
that need to be followed for me to get there, roughly.

1. Come out
    - I won't be able to do much if I'm still in the closet (God I hate that term). 
      Even just saying the word 'gay' feels like a cuss word that I'm not supposed to 
      say, so how am I supposed to have a conversation about this until I can own it?
2. Build Self-Confidence
    - I've gotta be able to have the balls to refute stereotypes, so this is a must for 
      the territory. I'm not sure how to accomplish this, though. I've been on a downward 
      spiral on this for years. During my high-school years specifically, I was in a total 
      disconnect socially, so it's been hard to build back from that. I have noticed though, 
      at least academically speaking, if I'm prepared, I can do well in delivering a point.
      A problem I've noticed, though, is improvisation. If I'm not ready for a specific 
      question, shit hits the fan. Some ways around this may be to get better at bullshitting 
      or to come up with good deflection strategies.
3. Be Informed
    - Any good argument requires good knowledge. As it stands now, I don't have a good knowledge
      of LGBTQ history, so this may be a good place to start. I have a decent grasp on 
      current political controversies. I'll need to stay up to date on these. The politics 
      thing will also be a tough balancing act though. If I read too much, I know it's going 
      to just leave me depressed as hell, so it'll need to be in moderation.

There's also some nice-to-haves that I should evaluate myself for. Notably...

 - Empathy
    - I'm not going to get anywhere in persuading someone if I dismiss their viewpoints 
      entirely. It'll be best to empathize with them, point out where in their view they're
      wrong, and suggest a suitable change.

 - Presentation
    - I can have all the right info and people skills in the world under my belt, but 
      those would all be at a loss if the presentation of my point was wrong. Points need 
      to be stated in a digestible manner that the listener expects.
</section>
<section markdown="1">

## GogoGadget Conclusion
I'm out of words to write. The well has dried up. Log off, go to bed. Goodnight.
![Lain from the anime Serial Experiments Lain logging off and cuddling up with her Blahaj](/assets/jpeg/lainhaj.jpg){:.centered}
</section>

