---
layout: page
titletag: Picobooting a GameCube
title: Picobooting a GameCube
desc: "Misadventures in installing an RPI Pico with PicoBoot in my roommate's GameCube featuring death, destruction, and rebirth. A tale for the ages."
permalink: /posts/blahajcube
tags: [gcn, consoles, modding]
mainimg: /assets/posts/blahajcube/fig16.jpg 
---
<section markdown="1">
## Intro

I've long held a love for jailbreaking video game consoles. In years prior, I've jailbroken PSPs, 3DSes, PS3s, Wiis, and more.
However, I've only once done a successful hard-mod of a console, my Nintendo Switch. The only other time I tried to 
hard-mod a console, an Xbox 360, I fried the thing. Given that I've been trying to get better at soldering lately and 
my roommate had a GameCube and four (!!!) controllers, I figured that it would be a good candidate to mod! 

</section>
<section markdown="1">

## Ordering Parts

I figured there'd be two main things I'd need for this project: a Raspberry Pi Pico and some way to connect an SD card
to the console. Thus, I went on eBay and purchased a Pico and an SD2SP2. My rationale behind ordering the Pico off of 
eBay was that I didn't have a Micro Center near by to purchase at MSRP and the rationale behind the SD2SP2 was that 
it was probably going to be the best fit for my roommate's DOL-001 console. I didn't purchase anything in the way of 
supplies as I already had a soldering iron, flux, and solder. The guide called for thinner gauge wire than what I had, 
but what's the worst that could happen?

</section>
<section markdown="1">

## The First Mistake

20 minutes later, I thought, "Huh, I should have actually checked that the GameCube has an SP2 port". I take off the cover 
and lo and behold, it doesn't. Turns out, the port is present on *most* DOL-001 consoles, but not all. I did some research 
after this discovery and found that this wouldn't be a huge problem. Turns out the board still has the points for the 
SP2 socket, they're just not populated. With more digging, I found that 0ctobogs[^1] on GBATemp had installed an SD2SP2 internally 
and only needed to add a single blob of solder to a point labeled FIL22 on the bottom of the board[^2]. I figured I'd take the 
capacitor that comes on the SD2SP2 board and create the adapter outlined by wolffangalchemist[^3] in the thread I found this comment on[^4]

[^1]: [0ctobogs on GBATemp](https://gbatemp.net/members/0ctobogs.171406/)
[^2]: [0ctobogs' post on installing the SD2SP2 in an SP2-less DOL-001](https://gbatemp.net/threads/diy-sd2sp2-installed-internally-soldering-a-micro-sd-adapter-to-gamecube-serial-port-2.550301/post-9629806)
[^3]: [wolffangalchemist on GBATemp](https://gbatemp.net/members/wolffangalchemist.144139/)
[^4]: [wolffangalchemist's guide for DIY'ing an SD2SP2](https://gbatemp.net/threads/diy-sd2sp2-installed-internally-soldering-a-micro-sd-adapter-to-gamecube-serial-port-2.550301/)

</section>
<section markdown="1">

## The First Attempt

Once the packages arrived, I heated up the soldering iron and got to work. First thing I did was prepping the SD card and the 
Pico. This was no hassle as it was mostly copying files around. Taking apart the console was  also no problem.
Since my roommate had kept it in good condition, there was no grime or gross stuff to clean up on the way in. 

![GameCube disassembled to the motherboard on my desk](/assets/posts/blahajcube/fig01.jpg){:.centered}

When selecting the wires I wanted to use, I initially thought I'd use an older CAT5 cable I had lying around. Unfortunately, 
I found it difficult to get the wires in this cable to tin up. I ended up using some thicker CAT6 cabling I had bought in 
bulk.

![The CAT6 cable in question](/assets/posts/blahajcube/fig02.jpg){:.centered}

Next, I began wiring. This went along swimmingly. After that, I did some short protection, i.e. I haphazardly wrapped the exposed
bits of wire with electrical tape.

![A few soldered wires](/assets/posts/blahajcube/fig03.jpg){:.centered}
![Few more, this time taped with red electrical tape](/assets/posts/blahajcube/fig04.jpg){:.centered}

Next was figuring out the SD2SP2. I didn't know the polarity of the capacitor on the SD2SP2 but what I did know was that 
the source for the board was available on Github. I downloaded the files and opened them in Kicad. This revealed the polarity. 

![Kicad schematic](/assets/posts/blahajcube/fig05.jpg){:.centered}
![SD2SP2 with the capacitor now removed](/assets/posts/blahajcube/fig06.jpg){:.centered}

Prepping the SD card was easy enough. 1 wire bridging two of the points, the capacitor in between some other points. 

![Prepped SD card. It's face-down on the table with a wire between two of the contacts.](/assets/posts/blahajcube/fig07.jpg){:.centered}

I also needed a way to mount the SD card. After speaking with my roommate, we decided mounting it above the Memory Card 1 slot
would be the best option. I used some clippers and a utility knife to cut open a hole and used some electrical tape to secure 
the SD card. It needed some widening after a test fit.

![Faceplate with a hole cut above slot 1](/assets/posts/blahajcube/fig08.jpg){:.centered}
![SD card haphazardly secured with electrical tape wrapped around the controller ports](/assets/posts/blahajcube/fig09.jpg){:.centered}
![The hole again, but a little wider](/assets/posts/blahajcube/fig10.jpg){:.centered}
![SD adapter fitted](/assets/posts/blahajcube/fig14.jpg){:.centered}

After that, the SD card adapter and the Pico were soldered in. It was far from pretty, but my connections looked OK. 

![Close-up of soldering work](/assets/posts/blahajcube/fig11.jpg){:.centered}
![Pic with the drive re-attached and the pico hanging off](/assets/posts/blahajcube/fig12.jpg){:.centered}

</section>
<section markdown="1">

## The First Test

Eager to see the results, I hooked up the GameCube to the TV and turned it on. It worked!

![GameCube booted into Swiss](/assets/posts/blahajcube/fig13.jpg){:.centered}

...for about 20 minutes. I wasn't sure why, but after about 3 on-off cycles in-between games, the console would 
no longer turn on. It would light up but not give any other signs of life. Thus, I took it apart and began 
searching for shorts. I couldn't find out what was wrong! At one point, I removed the Pico. GameCube would then 
power on. Huh. I tried shortening the wires to the Pico and resoldering. Somewhere in this process, I made a mistake 
and knocked a capacitor or something off of the Pico that was near the CPU, rendering it cooked. Ugh. Time to order more.
I went to DigiKey this time and ordered two Picos. 

![Close disassembly photo](/assets/posts/blahajcube/fig15.jpg){:.centered}
![Far disassembly photo](/assets/posts/blahajcube/fig16.jpg){:.centered}


</section>
<section markdown="1">

## The Second Attempt

Once the Pico arrived again, I got right back to work. I wired in the new one after programming and yippie! 
We're back in Swiss!

![Two Picos on a porcelain plate (for safety)](/assets/posts/blahajcube/fig17.jpg){:.centered}
![New Pico soldered in](/assets/posts/blahajcube/fig18.jpg){:.centered}
![In Swiss again!](/assets/posts/blahajcube/fig19.jpg){:.centered}

...for about 20 minutes. After a few more boots, the text on Swiss started rendering wrong or not at all. Then, it wouldn't
boot again.

![Swiss but fonts aren't being rendered](/assets/posts/blahajcube/fig20.jpg){:.centered}

Determined to get it working again, I started redoing all of my wiring. I desoldered the wires for SP2 and the Pico and found 
that the GameCube would still boot with them removed. Fantastic. I started wiring again. At one point I found that my solder 
wouldn't stick to my soldering iron any more. I wasn't sure why. Alas, I persisted. At one point, I was soldering the points 
that connect to that one surface mounted chip and just wasn't getting my way. The solder kept blobbing between feet and not 
joining the way I wanted. I persisted for a while until I made two big mistakes. I knocked off one foot and lifted the pad 
on another. Fuck. GameCube wouldn't boot anymore. I ended up facing the music and telling my roommate, who, thankfully, 
wasn't mad. I ordered another GameCube, this one listed for parts but with a working board, off of eBay and waited.

![Close up of wires soldered to SP2](/assets/posts/blahajcube/fig21.jpg){:.centered}
![Wires no longer soldered to SP2](/assets/posts/blahajcube/fig22.jpg){:.centered}
![GameCube booted to stock interface... for now.](/assets/posts/blahajcube/fig23.jpg){:.centered}
![Last photo of the board alive.](/assets/posts/blahajcube/fig24.jpg){:.centered}


</section>
<section markdown="1">

## Getting Educated

I didn't take this failure lightly. I wanted to find out *why* I failed. I've come to four conclusions. First, my wires 
were way too thick. I used 23awg when I should have been using 28awg like the guide states. Second, I wasn't using kapton 
tape. My wires weren't secured in any manner and I found my connections breaking because of stress put on the wires. Three, 
My short protection was awful. The electrical tape was prone to slipping. I needed a better solution. And four, my soldering 
iron's tip wasn't tinned. I found out that that's actually something that's really important when soldering. So, to address 
these, I did the following:

1. Wire Gauge
    - I searched all over town for thinner gauge wire but was unable to find any. I had some dupont wires laying
      about, so I prepared to use those. This probably wasn't the best solution. I'll need to order some high gauge 
      wire online in the future.
2. Kapton Tape
    - This is also something I didn't address this go-round. SAD!
3. Short Protection
    - I picked up some heat-shrink tubing to use. This was bigger than my wire thickness, but at least I could 
      put a cut up tube inside of another cut up tube to make something that could shrink around two wires.
4. Tip Tinning
    - I went to Harbor Freight and picked up a brass wool thing and some tip tinner. I played with it before the new 
      GameCube arrived and it massively improved the soldering experience. My iron was much easier to use in that the 
      solder now stuck to the tips again and the iron seemed to melt solder much faster.

</section>
<section markdown="1">

## The Third Attempt

At last, the new-to-us GameCube arrived. This thing was FILTHY! I ended up taking it apart and using the ultrasound cleaner 
at work to get the board cleaned up. The roommate also decided that they wanted their black housing mixed in with some of 
the purple parts from the new GameCube. I cleaned these parts up real good.

![New board!!](/assets/posts/blahajcube/fig25.jpg){:.centered}

Now it was time to solder. With newly tinned tips and (hopefully) thinner wire, I got to work. I worked more carefully this 
time in installing the Pico and the SD card. There were no major bumps in the road. I made sure to leave plenty of slack 
on the wires that connected to the SMD component as I didn't want to lift another pad and the wires were prone to come loose 
last time. I used some heat-shrink tubing to anchor them to the more firmly-attached ground connection. I also found 
that the SP2 wiring went a lot more swimmingly this time. I found I could insert fluxxed wires into the fluxxed holes and then 
apply solder which resulted in a quick and clean job. 


Finally, it was time to see if it worked (again). I hooked it up to the TV and YES! It's alive (again)!

![Swiss again](/assets/posts/blahajcube/fig26.jpg){:.centered}

...and it stayed alive! Fantastic! I buttoned it up and it was still working. Awesome.

</section>
<section class="bodybox" markdown="1">

## Blahajification

Earlier, while cleaning the housing for the GameCube, I made a discovery: The gem on the disk tray cover is removable! 
This gave me an idea: What if I made my own gem? I fired up Thingiverse and found a model for a replacement gem[^5]. I printed it 
and found that there were some inconsistencies in some tolerances, so I fixed those. Next, I embossed a blahaj graphic and the 
text "Blahaj Cube" into the gem. Using some drywall spackle and some white-out, I filled in this embossed graphic. It wasn't 
perfect, but it wasn't awful either. Finally, I used some see-through glue to attach the acryllic top layer of the old gem to 
this new insert. There were bubbles in the glue that appered, but hey, sharks live under water, right?

![GameCube with purple top buttons but a black body. The GameCube jewel is replaced with a blahaj one.](/assets/posts/blahajcube/fig27.jpg){:.centered}

I also found myself dissatisfied with Picoboot launching right into Swiss. That GameCube startup is iconic and I didn't want
it gone. Eventually, I stumbled across cubeboot AKA Flippyboot IPL[^6], an alternative to the Picoboot firmware that not only 
shows the GameCube startup before launching into Swiss but also allows you to change the color and the logo of the GameCube. 
Hooooooh boy. I launched into GIMP and created this: 

![GameCube startup, but it says BlahajCube instead of GameCube and it's blue.](/assets/posts/blahajcube/fig28.gif){:.centered}

[^5]: [GameCube custom jewel by Wizardof0z](https://www.thingiverse.com/thing:4894839/files)
[^6]: [cubeboot AKA Flippyboot IPL Github page](https://github.com/OffBroadway/cubeboot)

</section>
<section markdown="1">

## In Closing

I'm not one for writing conclusions but this project is something that definetley deserves one. 

 - What was lost
    - \$40 for a new GameCube board
    - \$5 plus shipping for a dead Pico
    - Countless hours lost to bad soldering

 - What was gained
   - A better understanding of soldering - The necessity of a tinned tip and what bearing that can have on a project
     as well as the importance of doing things right the first time to avoid costly mistakes in repetition
   - A modded GameCube (duh)
   - A chance to do a soldering project / a chance to use a Raspberry Pi Pico

While this was an expensive project in both time and money, I'm quite glad I did it. I learned more about soldering 
and have something to show for it too, at least until my roommate and I split.

</section>
